public class Client
{
    public static void main(String[] args)
    {
        Context context = new Context();
        context.setState(Context.State.one);

        context.methodA();
        context.methodB();
        context.methodB();
        context.methodA();
    }
}
