public class Context
{
    public static enum State
    {
        one, two
    }

    protected State state;

    public void methodA()
    {
        if (state == State.one)
        {
            System.out.println("MethodA doing something related with state one");
            state = State.two;
        }
        else
        {
            System.out.println("MethodA doing something related with state two");
            state = State.one;
        }
    }

    public void methodB()
    {
        if (state == State.one)
        {
            System.out.println("MethodB doing something related with state one");
            state = State.two;
        }
        else
        {
            System.out.println("MethodB doing something related with state two");
            state = State.one;
        }
    }

    public void setState(State state)
    {
        this.state = state;
    }
}
